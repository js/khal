khal v0.9.9 released (dependency clarification)
===============================================

.. feed-entry::
        :date: 2018-05-26

`khal v0.9.9`_ (and previous version of khal) currently only support the
dateutil library (that khal depends on) in versions < 2.7. The only change in
khal v0.9.9 is updated dependency.

If your OS already shipe dateutil >= 2.7, we recommend pipsi_ to install the
latest version of khal. 


Get `khal v0.9.9`_ from this site, or from pypi_.


.. _pypi: https://pypi.python.org/pypi/khal/
.. _khal v0.9.9: https://lostpackets.de/khal/downloads/khal-0.9.9.tar.gz
.. _pipsi: https://pypi.org/project/pipsi/

